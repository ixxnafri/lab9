//module control(	
//				input logic       CLK, 
//										RESET,
//										AES_START,
//										
//				output logic [2:0] SELECT_REG,
//				output logic [1:0] SELECT_IMC,				
//				output logic       AES_DONE,
//				output logic [3:0] ROUNDS
//);	
//	
//
//enum logic [5:0] {  WAIT, KEY_EXP, KEY_EXP_WAIT, ADD_ROUND_1, INV_SHIFT_ROW, INV_SUB_BYTE,ADD_ROUND_2, 
//						INV_MIX_COL_1, INV_MIX_COL_2, INV_MIX_COL_3, INV_MIX_COL_4, INV_MIX_COL_5, 
//						//INV_MIX_COL_6, INV_MIX_COL_7, 
//						INV_SHIFT_ROW3, INV_SUB_BYTE3, ADD_ROUND_KEY3, DONE} State, Next_state;  // Internal state logic
//	
//	logic [3:0] KEY_COUNTER = 0;
//	logic [3:0] ROUNDS_COUNT = 0;
//	
//	always_ff @ (posedge CLK)
//	begin
//		if (RESET) 
//			State <= WAIT;
//		else
//			State <= Next_state;
//	end
//   
//always_comb 
//begin
//
//	//Default next state is staying at current state
//
//	Next_state = State;
//	
//	//Default control signal values
//	
//	
//	//Assign next State
//	unique case (State)
//			WAIT:
//			begin
//				if(AES_START)
//					Next_state = KEY_EXP;
//
//				else
//					Next_state = WAIT;
//			end
//			KEY_EXP:
//			begin	
//				Next_state =KEY_EXP_WAIT;
//			end	
//			KEY_EXP_WAIT:
//			begin
//				if(KEY_COUNTER < 10)
//					Next_state =KEY_EXP_WAIT;
//				else
//					Next_state =ADD_ROUND_1;
//			end
//
/////////////////////////////////////////////////////////////
////																			//
////								First Round								//
////																			//
/////////////////////////////////////////////////////////////	
//					
//			ADD_ROUND_1:
//				Next_state =INV_SHIFT_ROW;
//				
//
/////////////////////////////////////////////////////////////
////																			//
////								LOOPY LOOP								//
////																			//
/////////////////////////////////////////////////////////////			
//			
//			INV_SHIFT_ROW:
//				Next_state = INV_SUB_BYTE;
//				
//				
//			INV_SUB_BYTE:
//				Next_state =ADD_ROUND_2;
//				
//				
//			ADD_ROUND_2:
//				Next_state = INV_MIX_COL_1;
//							
//			INV_MIX_COL_1:
//				Next_state = INV_MIX_COL_2;
//	
//			INV_MIX_COL_2:
//				Next_state = INV_MIX_COL_3;
//				
//			INV_MIX_COL_3:
//				Next_state = INV_MIX_COL_4;
//				
//			INV_MIX_COL_4:
//				Next_state = INV_MIX_COL_5;
//				
////			INV_MIX_COL_5:
////				Next_state = INV_MIX_COL_6;
////				
////				
////			INV_MIX_COL_6:
////				Next_state = INV_MIX_COL_7;
//				
//				
//			INV_MIX_COL_5:
//			begin
//				if(ROUNDS_COUNT < 11)
//					Next_state = INV_SHIFT_ROW;
//				else
//					Next_state = INV_SHIFT_ROW3;
//			end	
//
/////////////////////////////////////////////////////////////
////																			//
////								Last Round								//
////																			//
///////////////////////////// ////////////////////////////////			
//
//			INV_SHIFT_ROW3:
//				Next_state = INV_SUB_BYTE3;
//
//		
//			
//			INV_SUB_BYTE3:
//				Next_state = ADD_ROUND_KEY3;
//			
//		
//			ADD_ROUND_KEY3:
//				Next_state = DONE;
//	
//	
//			DONE:
//			begin
//				if(AES_START == 0)
//					Next_state = WAIT;
//				else	
//					Next_state = DONE;
//			end
//			
//			default: ;
//	endcase
//	
//	case(State)
//		WAIT: ;
//		KEY_EXP: ;
//		ADD_ROUND_1: ;
//		
//		
//		INV_SHIFT_ROW: ;
//		INV_SUB_BYTE: ;
//		ADD_ROUND_2: ;
//		INV_MIX_COL_1: ;
//		INV_MIX_COL_2: ;
//		INV_MIX_COL_3: ;
//		INV_MIX_COL_4: ;
//		INV_MIX_COL_5: ;
////		INV_MIX_COL_6: ;
////		INV_MIX_COL_7: ;
//		
//		
//		INV_SHIFT_ROW3:;
//		INV_SUB_BYTE3: ;
//		ADD_ROUND_KEY3:;
//		DONE:				;
//
//		
//		default:;
//	endcase
//end
//	
//endmodule
