module MUX5_1(
		input logic[127:0] D0, D1, D2, D3, D4,
		input logic [2:0] Select,
		output logic[127:0] Y);
		
		always_comb
		begin
			
			case(Select)
			3'b100:
					Y = D4; 

			3'b011:
					Y = D3; 
					
			3'b010:
					Y = D2;
					
		   3'b001:
					Y = D1;	
					
			3'b000:
					Y = D0;
		endcase
	end
		
endmodule


module DEMUX(
		input logic[31:0] DEMUX_INPUT,
		input logic [1:0] SELECT_IMC,
		output logic[31:0] TO_MUX_IMC);
		
		always_comb begin
	case(SELECT_IMC)
		2'b00:
			TO_MUX_IMC[31:0] = DEMUX_INPUT;
		2'b01:
			TO_MUX_IMC[63:32] = DEMUX_INPUT;
		2'b10:
			 TO_MUX_IMC[95:64] = DEMUX_INPUT;
		2'b11:
			TO_MUX_IMC[127:96] = DEMUX_INPUT;
	endcase
end	
		
endmodule
