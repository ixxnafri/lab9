/************************************************************************
AES Decryption Core Logic

Dong Kai Wang, Fall 2017

For use with ECE 385 Experiment 9
University of Illinois ECE Department
************************************************************************/

module AES (
	input	 logic CLK,
	input  logic RESET,
	input  logic AES_START,
	output logic AES_DONE,
	input  logic [127:0] AES_KEY,
	input  logic [127:0] AES_MSG_ENC,
	output logic [127:0] AES_MSG_DEC
);

logic [4:0] KEY_COUNTER;

///////////////////////////////////////////////////////////
//																			//
//								Key Expansion							//
//																			//
///////////////////////////////////////////////////////////
	
KeyExpansion key_exp(
	.clk(CLK),
	.Cipherkey(AES_KEY),
	.KeySchedule(KeySchedule)
 );
 
 
///////////////////////////////////////////////////////////
//																			//
//							Add Round Key  							//
//																			//
///////////////////////////////////////////////////////////	

/**
a    	logic[127:0] TEMP_OUTPUT; 
b		logic[127:0] TEMP_MUX_ARK;	 	
c		logic[127:0] TEMP_MUX_ISR;
d		logic[127:0] TEMP_MUX_IMC;
e		logic[127:0] TEMP_RETURN;
**/
AddRoundKey R_1(
	.AES_MSG_ENC(),
	.KeySchedule(KeySchedule),
	.numRounds(ROUNDS),
	.State(TO_MUX_ADDROUNDKEY)
);


always_ff @ (posedge CLK)
	begin
	if (~COUNT) 
		KEY_COUNTER <= 5'd0;
	else 
		KEY_COUNTER <= KEY_COUNTER +1;
	end
endmodule




