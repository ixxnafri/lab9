//module AddRoundKey(
//						input logic [127:0]  AES_MSG_ENC,
//						input logic [1407:0] KeySchedule,
//						input logic [3:0]    numRounds,
//						output logic[127:0]  State);
//
//						
//logic [127:0]add_round_key_output;
//
//always_comb
//	begin
//	
//	case(numRounds)
//		4'd1:
//			add_round_key_output = (KeySchedule[((128*1)-1):((128*1)-128)])^(AES_MSG_ENC); 
//		4'd2:
//			add_round_key_output = (KeySchedule[((128*2)-1):((128*2)-128)])^(AES_MSG_ENC); 
//		4'd3:
//			add_round_key_output = (KeySchedule[((128*3)-1):((128*3)-128)])^(AES_MSG_ENC); 
//		4'd4:
//			add_round_key_output = (KeySchedule[((128*4)-1):((128*4)-128)])^(AES_MSG_ENC); 
//		4'd5:
//			add_round_key_output = (KeySchedule[((128*5)-1):((128*5)-128)])^(AES_MSG_ENC); 
//		4'd6:
//			add_round_key_output = (KeySchedule[((128*6)-1):((128*6)-128)])^(AES_MSG_ENC); 
//		4'd7:
//			add_round_key_output = (KeySchedule[((128*7)-1):((128*7)-128)])^(AES_MSG_ENC); 
//		4'd8:
//			add_round_key_output = (KeySchedule[((128*8)-1):((128*8)-128)])^(AES_MSG_ENC); 
//		4'd9:
//			add_round_key_output = (KeySchedule[((128*9)-1):((128*9)-128)])^(AES_MSG_ENC); 
//		4'd10:
//			add_round_key_output = (KeySchedule[((128*10)-1):((128*10)-128)])^(AES_MSG_ENC); 
//		4'd11:
//			add_round_key_output = (KeySchedule[((128*11)-1):((128*11)-128)])^(AES_MSG_ENC); 
//		default:
//			add_round_key_output = (KeySchedule[((128*1)-1):((128*1)-128)])^(AES_MSG_ENC); 
//	endcase
//	end
//	
//assign State = add_round_key_output;
//
//endmodule
