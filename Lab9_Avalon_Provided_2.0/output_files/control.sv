//module ctrl(	
//				input logic       CLK, 
//										RESET,
//										AES_START,
//										
//				output logic [2:0] SELECT_REG,
//				output logic [1:0] SELECT_IMC,				
//				output logic       AES_DONE,
//				output logic [3:0] ROUNDS
//);	
//	
//enum logic [5:0] {  	WAIT, 
//							KEY_EXP, 
//							KEY_EXP_WAIT, 
//							ADD_ROUND_1, 
//							INV_SHIFT_ROW, 
//							INV_SUB_BYTE,
//							INV_SUB_BYTE_1, 
//							INV_SUB_BYTE_2,
//							ADD_ROUND_2, 
//							INV_MIX_COL_1, 
//							INV_MIX_COL_2, 
//							INV_MIX_COL_3, 
//							INV_MIX_COL_4, 
//							INV_MIX_COL_5, 
//							INV_SHIFT_ROW_3,
//							INV_SUB_BYTE_3,
//							INV_SUB_BYTE_4,
//							INV_SUB_BYTE_5, 
//							ADD_ROUND_KEY_3, 
//							DONE} State, Next_state;  // Internal state logic
//	
//	logic [3:0] KEY_COUNTER = 0;
//	logic [3:0] ROUNDS_COUNT = 0;
//	logic [5:0] KEY_COUNTER_NEW;
//	
//	always_ff @ (posedge CLK)
//	begin
//		if (RESET) 
//			State <= WAIT;
//		else
//			State <= Next_state;
//			KEY_COUNTER <= KEY_COUNTER_NEW;
//	end
//   
//always_comb 
//
//begin
//	//Default next state is staying at current state
//	Next_state = State;	
//	//Default control signal values
//	AES_DONE = 1'b0;
//	SELECT_REG = 3'b000;
//	SELECT_IMC = 2'b00;
//	ROUNDS = 4'b0000;
//	KEY_COUNTER_NEW = 5'b00000;
//	
//	//Assign next State
//	unique case (State)
//	
//			WAIT:
//			begin
//				if(AES_START)
//					Next_state = KEY_EXP;
//
//				else
//					Next_state = WAIT;
//			end
//			
//			KEY_EXP:
//				Next_state =KEY_EXP_WAIT;	
//			
//			
//			KEY_EXP_WAIT:
//			begin
//				if(KEY_COUNTER < 20)
//					Next_state = KEY_EXP_WAIT;
//				else
//					Next_state =ADD_ROUND_1;
//			end
//
/////////////////////////////////////////////////////////////
////																			//
////								First Round								//
////																			//
/////////////////////////////////////////////////////////////	
//					
//			ADD_ROUND_1:
//				Next_state =INV_SHIFT_ROW;
//				
//				
/////////////////////////////////////////////////////////////
////																			//
////								LOOPY LOOP								//
////																			//
/////////////////////////////////////////////////////////////			
//			
//			INV_SHIFT_ROW:
//				Next_state = INV_SUB_BYTE;
//				
//				
//			INV_SUB_BYTE:
//				Next_state =INV_SUB_BYTE_1;
//				
//			INV_SUB_BYTE_1:
//				Next_state =INV_SUB_BYTE_2;
//				
//			INV_SUB_BYTE_2:
//				Next_state =ADD_ROUND_2;
//	
//			ADD_ROUND_2:
//				Next_state = INV_MIX_COL_1;
//							
//			INV_MIX_COL_1:
//				Next_state = INV_MIX_COL_2;
//	
//			INV_MIX_COL_2:
//				Next_state = INV_MIX_COL_3;
//				
//			INV_MIX_COL_3:
//				Next_state = INV_MIX_COL_4;
//				
//			INV_MIX_COL_4:
//				Next_state = INV_MIX_COL_5;
//				
//				
//			INV_MIX_COL_5:
//			begin
//				if(ROUNDS_COUNT < 11)
//					Next_state = INV_SHIFT_ROW;
//				else
//					Next_state = INV_SHIFT_ROW_3;
//			end	
//
/////////////////////////////////////////////////////////////
////																			//
////								Last Round								//
////																			//
///////////////////////////// ////////////////////////////////			
//
//			INV_SHIFT_ROW_3:
//				Next_state = INV_SUB_BYTE_3;
//				
//			INV_SUB_BYTE_3:
//				Next_state = INV_SUB_BYTE_4;
//			INV_SUB_BYTE_4:
//				Next_state = INV_SUB_BYTE_5;
//			INV_SUB_BYTE_5:
//				Next_state = ADD_ROUND_KEY_3;
//			
//		
//			ADD_ROUND_KEY_3:
//				Next_state = DONE;
//	
//	
//			DONE:
//			begin
//				if(AES_START == 0)
//					Next_state = WAIT;
//				else	
//					Next_state = DONE;
//			end
//			
//			default: ;
//	endcase
//	
//	case(State)
//	
//				WAIT:
//					AES_DONE = 1'b0;
//				KEY_EXP:
//				begin 
//				SELECT_REG = 3'b101;
//				end
//				KEY_EXP_WAIT:
//				begin
//					KEY_COUNTER_NEW = KEY_COUNTER_NEW + 1;
//					ROUNDS = KEY_COUNTER_NEW;
//				end			
//				ADD_ROUND_1:
//					SELECT_REG = 3'b000;
//						
//				INV_SHIFT_ROW:; 
//							INV_SUB_BYTE:;
//							INV_SUB_BYTE_1:;
//							INV_SUB_BYTE_2:;
//							ADD_ROUND_2:;
//							INV_MIX_COL_1:; 
//							INV_MIX_COL_2:; 
//							INV_MIX_COL_3:; 
//							INV_MIX_COL_4:; 
//							INV_MIX_COL_5:; 
//							INV_SHIFT_ROW_3:;
//							INV_SUB_BYTE_3:;
//							INV_SUB_BYTE_4:;
//							INV_SUB_BYTE_5:; 
//							ADD_ROUND_KEY_3:;
//			
//		default:;
//	endcase
//end
//	
//endmodule
