/************************************************************************
AES Decryption Core Logic

Dong Kai Wang, Fall 2017

For use with ECE 385 Experiment 9
University of Illinois ECE Department
************************************************************************/

module AES (
	input	 logic CLK,
	input  logic RESET,
	input  logic AES_START,
	output logic AES_DONE,
	input  logic [127:0] AES_KEY,
	input  logic [127:0] AES_MSG_ENC,
	output logic [127:0] AES_MSG_DEC
);


logic [127:0] TEMP;
logic [1407:0] KeySchedule;
logic [127:0] TO_MUX_ADDROUNDKEY;
logic [127:0] TO_MUX_INVSUBBYTES;
logic [127:0] TO_MUX_SHIFTROWS;
logic [127:0] TO_MUX_IMC;

logic [31:0] IMC_INPUT;
logic [31:0] IMC_OUTPUT;

logic [3:0] ROUNDS;
logic [1:0] SELECT_IMC;
logic [2:0] SELECT_REG;


ctrl control(	
				.CLK(CLK), 
				.RESET(RESET),
				.AES_START(AES_START),
				.SELECT_REG(SELECT_REG),
				.SELECT_IMC(SELECT_IMC),				
				.AES_DONE(AES_DONE),
				.ROUNDS(ROUNDS));
				


///////////////////////////////////////////////////////////
//																			//
//								Key Expansion							//
//																			//
///////////////////////////////////////////////////////////
	
KeyExpansion key_exp(
	.clk(CLK),
	.Cipherkey(AES_KEY),
	.KeySchedule(KeySchedule)
 );

 		
///////////////////////////////////////////////////////////
//																			//
//							Add Round Key  							//
//																			//
///////////////////////////////////////////////////////////	


AddRoundKey R_1(
	.AES_MSG_ENC(TEMP),
	.KeySchedule(KeySchedule),
	.numRounds(ROUNDS),
	.State(TO_MUX_ADDROUNDKEY)
);
 

///////////////////////////////////////////////////////////
//																			//
//				         Inverse Shift Row						   //
//																			//
///////////////////////////////////////////////////////////	

// INVERSE SHIFT ROW WORKS (Y)
InvShiftRows inv_shift(.data_in(TEMP), .data_out(TO_MUX_SHIFTROWS));


///////////////////////////////////////////////////////////
//																			//
//							Inverse SubByte							//
//																			//
///////////////////////////////////////////////////////////	

// INVERSE SUBBYTE (ON NEGATIVE EDGE)  ADD ONE BUFFER	
logic [127:0] output_sub_byte;

	InvSubBytes inv_sub0  (.clk(CLK), .in(TEMP[7:0]    ), .out( output_sub_byte [7:0]    ));
	InvSubBytes inv_sub1  (.clk(CLK), .in(TEMP[15:8]   ), .out( output_sub_byte [15:8]   ));
	InvSubBytes inv_sub2  (.clk(CLK), .in(TEMP[23:16]  ), .out( output_sub_byte [23:16]  ));
	InvSubBytes inv_sub3  (.clk(CLK), .in(TEMP[31:24]  ), .out( output_sub_byte [31:24]  ));
	InvSubBytes inv_sub4  (.clk(CLK), .in(TEMP[39:32]  ), .out( output_sub_byte [39:32]  ));
	InvSubBytes inv_sub5  (.clk(CLK), .in(TEMP[47:40]  ), .out( output_sub_byte [47:40]  ));
	InvSubBytes inv_sub6  (.clk(CLK), .in(TEMP[55:48]  ), .out( output_sub_byte [55:48]  ));
	InvSubBytes inv_sub7  (.clk(CLK), .in(TEMP[63:56]  ), .out( output_sub_byte [63:56]  ));
	InvSubBytes inv_sub8  (.clk(CLK), .in(TEMP[71:64]  ), .out( output_sub_byte [71:64]  ));
	InvSubBytes inv_sub9  (.clk(CLK), .in(TEMP[79:72]  ), .out( output_sub_byte [79:72]  ));
	InvSubBytes inv_sub10 (.clk(CLK), .in(TEMP[87:80]  ), .out( output_sub_byte [87:80]  ));
	InvSubBytes inv_sub11 (.clk(CLK), .in(TEMP[95:88]  ), .out( output_sub_byte [95:88]  ));
	InvSubBytes inv_sub12 (.clk(CLK), .in(TEMP[103:96] ), .out( output_sub_byte [103:96] ));
	InvSubBytes inv_sub13 (.clk(CLK), .in(TEMP[111:104]), .out( output_sub_byte [111:104]));
	InvSubBytes inv_sub14 (.clk(CLK), .in(TEMP[119:112]), .out( output_sub_byte [119:112]));
	InvSubBytes inv_sub15 (.clk(CLK), .in(TEMP[127:120]), .out( output_sub_byte [127:120]));
	
assign TO_MUX_INVSUBBYTES = output_sub_byte;

///////////////////////////////////////////////////////////
//																			//
//							InvMixColumns								//
//																			//
///////////////////////////////////////////////////////////	


always_comb
begin 
	IMC_INPUT = 0;
	case(SELECT_IMC)
		2'd0:
			IMC_INPUT = TEMP[31:0];
		2'd1:
			IMC_INPUT = TEMP[63:32];
		2'd2:
			IMC_INPUT = TEMP[95:64];
		2'd3:
			IMC_INPUT = TEMP[127:96];
	endcase
end

InvMixColumns Inv_Mix(
	.in(IMC_INPUT),
	.out(IMC_OUTPUT)
);

always_comb
begin 
	TO_MUX_IMC = 0;
	case(SELECT_IMC)
		2'd0:
			TO_MUX_IMC[31:0]   = IMC_OUTPUT;
		2'd1:
			TO_MUX_IMC[63:32]  = IMC_OUTPUT;
		2'd2:
			TO_MUX_IMC[95:64]  = IMC_OUTPUT;
		2'd3:
			TO_MUX_IMC[127:96] = IMC_OUTPUT;
	endcase
end

MUX5_1 Work_Bitch( 
			.TO_MUX_ADDROUNDKEY(TO_MUX_ADDROUNDKEY), 
			.TO_MUX_INVSUBBYTES(TO_MUX_INVSUBBYTES), 
			.TO_MUX_SHIFTROWS(TO_MUX_SHIFTROWS), 
			.TO_MUX_IMC(TO_MUX_IMC), 
			.TEMP_1(TEMP),
			.AES_MSG_ENC(AES_MSG_ENC),
			.SELECT_REG(SELECT_REG),
			.TEMP(TEMP));

assign AES_MSG_DEC = TEMP;
endmodule


	//////////////////////////////////////////////////////////////////////////////////////////
	//		//			//		 /////////	   ////////		  //		 //	 //		    ////////	 //
	//		////	  ///		//			//	   //		 //	  //		 //	 //			 //			 //
	//		//	//	 ////		//			//	   //		  //    //		 //	 //			 //			 //
	//		//	 ////	//		//			//	   //		  //	  //		 //	 //			 //			 //
	//		//	  //	//		//			//	   //		  //    //	    //	 //			 ///////	    //
	//		//			//		//			//	   //		  //	  //	    // 	 //		    //			 //
	//		//			//		//			//	   //		  //	  //		 //	 //			 //		  	 //
	//		//			//		//			//	   //		 //     //		 //	 //			 //			 //
	//		//			//		 /////////	   ////////			////////		 ////////	 ////////	 //
	//////////////////////////////////////////////////////////////////////////////////////////


module ctrl(	
				input logic       CLK, 
										RESET,
										AES_START,
										
				output logic [2:0] SELECT_REG,
				output logic [1:0] SELECT_IMC,				
				output logic       AES_DONE,
				output logic [3:0] ROUNDS
);	
	
enum logic [5:0] {  	WAIT, 
							INIT,
							KEY_EXP, 
							KEY_EXP_WAIT,
							KEY_EXP_WAIT_2,
							ADD_ROUND_1, 
							INV_SHIFT_ROW, 
							INV_SUB_BYTE,
							INV_SUB_BYTE_1, 
							INV_SUB_BYTE_2,
							ADD_ROUND_2, 
							INV_MIX_COL_1, 
							INV_MIX_COL_2, 
							INV_MIX_COL_3, 
							INV_MIX_COL_4, 
							INV_MIX_COL_5, 
							INV_SHIFT_ROW_3,
							INV_SUB_BYTE_3,
							INV_SUB_BYTE_4,
							INV_SUB_BYTE_5, 
							ADD_ROUND_KEY_3, 
							DONE} State, Next_state;  // Internal state logic
	logic COUNT;
	logic [4:0] KEY_COUNTER;
	
//	always_ff @ (posedge CLK)
//	begin
//	if (~COUNT) 
//		KEY_COUNTER <= 5'd0;
//	else 
//		KEY_COUNTER <= KEY_COUNTER +1;
//	end



	always_ff @ (posedge CLK)
	begin
		if (RESET) 
			State <= WAIT;
		else
			begin
			State <= Next_state;
			end
	end
   
always_comb 

begin
	//Default next state is staying at current state
	Next_state = State;	
	//Default control signal values
	AES_DONE = 1'b0;
	SELECT_REG = 3'b000;
	SELECT_IMC = 2'b00;
	ROUNDS = 3'b0001;
	COUNT = 0;
	
	//Assign next State
	unique case (State)
	
			WAIT:
			begin
				if(AES_START)
					Next_state = INIT;

				else
					Next_state = WAIT;
			end
			
			INIT:
				Next_state =KEY_EXP;	
			
			KEY_EXP:
				Next_state =KEY_EXP_WAIT;	
			
			
			KEY_EXP_WAIT:
//			begin
//				if(KEY_COUNTER < 5'd8)
			//		Next_state = KEY_EXP_WAIT_2;
//				else
					Next_state =ADD_ROUND_1;
	//		end
			
			KEY_EXP_WAIT_2:
				Next_state = KEY_EXP_WAIT;

///////////////////////////////////////////////////////////
//																			//
//								First Round								//
//																			//
///////////////////////////////////////////////////////////	
					
			ADD_ROUND_1:
				Next_state =INV_SHIFT_ROW;
				
				
///////////////////////////////////////////////////////////
//																			//
//								LOOPY LOOP								//
//																			//
///////////////////////////////////////////////////////////			
			
			INV_SHIFT_ROW:
				Next_state = INV_SUB_BYTE;
				
				
			INV_SUB_BYTE:
				Next_state =INV_SUB_BYTE_1;
				
			INV_SUB_BYTE_1:
				Next_state =INV_SUB_BYTE_2;
				
			INV_SUB_BYTE_2:
				Next_state =ADD_ROUND_2;
	
			ADD_ROUND_2:
				Next_state = INV_MIX_COL_1;
							
			INV_MIX_COL_1:
				Next_state = INV_MIX_COL_2;
	
			INV_MIX_COL_2:
				Next_state = INV_MIX_COL_3;
				
			INV_MIX_COL_3:
				Next_state = INV_MIX_COL_4;
				
			INV_MIX_COL_4:
				Next_state = INV_MIX_COL_5;
				
				
			INV_MIX_COL_5:
			begin
				//if(ROUNDS < 11)
			//		Next_state = INV_SHIFT_ROW;
			//else
					Next_state = INV_SHIFT_ROW_3;
			end	

///////////////////////////////////////////////////////////
//																			//
//								Last Round								//
//																			//
/////////////////////////// ///////////////////////////////			

			INV_SHIFT_ROW_3:
				Next_state = INV_SUB_BYTE_3;
				
			INV_SUB_BYTE_3:
				Next_state = INV_SUB_BYTE_4;
			INV_SUB_BYTE_4:
				Next_state = INV_SUB_BYTE_5;
			INV_SUB_BYTE_5:
				Next_state = ADD_ROUND_KEY_3;
			
		
			ADD_ROUND_KEY_3:
				Next_state = DONE;
	
	
			DONE:
			begin
				if(AES_START == 0)
					Next_state = WAIT;
				else	
					Next_state = DONE;
			end
			
			default: ;
	endcase
	
	case(State)
	
				WAIT:
				begin
					AES_DONE = 1'b0;
					COUNT = 0;
				end
				INIT:
				begin
					COUNT = 1;
					SELECT_REG = 3'b101;
				end	
				KEY_EXP:
				begin
					 COUNT =1;	
				    SELECT_REG = 3'b101;
				end
				KEY_EXP_WAIT:
				begin
					 COUNT =1;
					 SELECT_REG = 3'b101;
				end
				KEY_EXP_WAIT_2:
				begin
					SELECT_REG = 3'b101;
					COUNT =1;
				end
				ADD_ROUND_1:
				begin
					SELECT_REG = 3'b000;
					COUNT =0;
				end		
				INV_SHIFT_ROW:
				begin
					SELECT_REG = 3'b010;
				end
				INV_SUB_BYTE:
					SELECT_REG = 3'b001;
				INV_SUB_BYTE_1:
					SELECT_REG = 3'b001;
				INV_SUB_BYTE_2:
					SELECT_REG = 3'b001;	
				ADD_ROUND_2:
					SELECT_REG = 3'b000;
				INV_MIX_COL_1:
					SELECT_REG = 3'b011; 
				INV_MIX_COL_2:
					SELECT_REG = 3'b011; 
				INV_MIX_COL_3:
					SELECT_REG = 3'b011; 
				INV_MIX_COL_4:
					SELECT_REG = 3'b011; 	
				INV_MIX_COL_5:
					SELECT_REG = 3'b011; 
				INV_SHIFT_ROW_3:
					SELECT_REG = 3'b010;
				INV_SUB_BYTE_3:
					SELECT_REG = 3'b001;
				INV_SUB_BYTE_4:;
					//SELECT_REG = 3'b001;
				INV_SUB_BYTE_5:;
					//SELECT_REG = 3'b001;
				ADD_ROUND_KEY_3:;
					//SELECT_REG = 3'b000;
		default:;
	endcase
end
	
endmodule

module AddRoundKey(
						input logic [127:0]  AES_MSG_ENC,
						input logic [1407:0] KeySchedule,
						input logic [3:0]    numRounds,
						output logic[127:0]  State);

						
logic [127:0]add_round_key_output;

always_comb
	begin
	
	case(numRounds)
		4'd1:
			add_round_key_output = (KeySchedule[((128*1)-1):((128*1)-128)])^(AES_MSG_ENC); 
		4'd2:
			add_round_key_output = (KeySchedule[((128*2)-1):((128*2)-128)])^(AES_MSG_ENC); 
		4'd3:
			add_round_key_output = (KeySchedule[((128*3)-1):((128*3)-128)])^(AES_MSG_ENC); 
		4'd4:
			add_round_key_output = (KeySchedule[((128*4)-1):((128*4)-128)])^(AES_MSG_ENC); 
		4'd5:
			add_round_key_output = (KeySchedule[((128*5)-1):((128*5)-128)])^(AES_MSG_ENC); 
		4'd6:
			add_round_key_output = (KeySchedule[((128*6)-1):((128*6)-128)])^(AES_MSG_ENC); 
		4'd7:
			add_round_key_output = (KeySchedule[((128*7)-1):((128*7)-128)])^(AES_MSG_ENC); 
		4'd8:
			add_round_key_output = (KeySchedule[((128*8)-1):((128*8)-128)])^(AES_MSG_ENC); 
		4'd9:
			add_round_key_output = (KeySchedule[((128*9)-1):((128*9)-128)])^(AES_MSG_ENC); 
		4'd10:
			add_round_key_output = (KeySchedule[((128*10)-1):((128*10)-128)])^(AES_MSG_ENC); 
		4'd11:
			add_round_key_output = (KeySchedule[((128*11)-1):((128*11)-128)])^(AES_MSG_ENC); 
		default:
			add_round_key_output = (KeySchedule[((128*1)-1):((128*1)-128)])^(AES_MSG_ENC); 
	endcase
	end
	
assign State = add_round_key_output;

endmodule


module MUX5_1(
		input logic[127:0] TO_MUX_ADDROUNDKEY, TO_MUX_INVSUBBYTES, TO_MUX_SHIFTROWS, TO_MUX_IMC, TEMP_1, AES_MSG_ENC,
		input logic [2:0] SELECT_REG,
		output logic[127:0] TEMP);
	
	
always_comb
begin 
	case(SELECT_REG)
		3'b000:
			TEMP = TO_MUX_ADDROUNDKEY;
		3'b001:
			TEMP = TO_MUX_INVSUBBYTES;
		3'b010:
			TEMP = TO_MUX_SHIFTROWS;
		3'b011:
			TEMP = TO_MUX_IMC;
		3'b100:
			TEMP = TEMP_1;  
		3'b101:
			TEMP = AES_MSG_ENC;
		default:
			TEMP = TEMP_1;
	endcase
end

endmodule




