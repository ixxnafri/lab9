/************************************************************************
Lab 9 Nios Software

Dong Kai Wang, Fall 2017
Christine Chen, Fall 2013

For use with ECE 385 Experiment 9
University of Illinois ECE Department
************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "aes.h"
#define Nk 4
#define Nr 10
#define Nb 4

// Pointer to base address of AES module, make sure it matches Qsys
volatile unsigned int * AES_PTR = (unsigned int *) 0x00000040;

// Execution mode: 0 for testing, 1 for benchmarking
int run_mode = 0;

/** charToHex
 *  Convert a single character to the 4-bit value it represents.
 *  
 *  Input: a character c (e.g. 'A')
 *  Output: converted 4-bit value (e.g. 0xA)
 */
char charToHex(char c)
{
	char hex = c;

	if (hex >= '0' && hex <= '9')
		hex -= '0';
	else if (hex >= 'A' && hex <= 'F')
	{
		hex -= 'A';
		hex += 10;
	}
	else if (hex >= 'a' && hex <= 'f')
	{
		hex -= 'a';
		hex += 10;
	}
	return hex;
}

/** charsToHex
 *  Convert two characters to byte value it represents.
 *  Inputs must be 0-9, A-F, or a-f.
 *  
 *  Input: two characters c1 and c2 (e.g. 'A' and '7')
 *  Output: converted byte value (e.g. 0xA7)
 */
char charsToHex(char c1, char c2)
{
	char hex1 = charToHex(c1);
	char hex2 = charToHex(c2);
	return (hex1 << 4) + hex2;
}


void RotWord(unsigned char* word){
	unsigned char t = word[0];
	word[0] = word[1];
	word[1] = word[2];
	word[2] = word[3];
	word[3] = t;
}

void SubWord(unsigned char* state){
	int i;
	for(i = 0; i < 4; i++)
	{
		state[i] = aes_sbox[state[i]];
	}
}

void KeyExpansion(unsigned char* key, unsigned char* plain_text) {
	unsigned char temp[4];
	int i,j,k;
	for (i = 0; i < Nk; i++) {
		for (j = 0; j < 4; j++){
			plain_text[4*i+j] = key[4*i+j];
		}
	}
	for (i = Nk; i < Nb*((Nr+1)); i++){
		for (j = 0; j < 4; j++){
			temp[j] = plain_text[4*(i-1)+j];
		}

		if(i % Nk == 0){
            RotWord(temp);
            SubWord(temp);
			for(k = 0; k < 4; k++){
				if(k == 0){
				    temp[k] = (unsigned int)temp[k] ^ (Rcon[i/4]>>24);
				    }
                temp[k] = (unsigned int)temp[k] ^ 0;
			}
		}

		for(j = 0; j < 4; j++) {
			plain_text[4*i+j] = temp[j] ^ plain_text[4*(i-4)+j];
		}
	}
}


void SubBytes(unsigned char* state){
	int i;
	for(i = 0; i < 16; i++)
	{
		state[i] = aes_sbox[(uint)state[i]];
	}
}


void ShiftRows(unsigned char* state){
	// first row does not rotate
	// second row rotate 1
	// third row rotate 2
	// last row rotate 3
	unsigned char tmp[16];
	 tmp[0] = state[0];
	 tmp[1] = state[5];
	 tmp[2] = state[10];
	 tmp[3] = state[15];
	 tmp[4] = state[4];
	 tmp[5] = state[9];
	 tmp[6] = state[14];
	 tmp[7] = state[3];
	 tmp[8] = state[8];
	 tmp[9] = state[13];
	 tmp[10] = state[2];
	 tmp[11] = state[7];
	 tmp[12] = state[12];
	 tmp[13] = state[1];
	 tmp[14] = state[6];
	 tmp[15] = state[11];

	int i ;
	for(i = 0; i < 16; i++){
		state[i] = tmp[i];
	}
}

void print(uchar* input){
	int i;
	printf("the printed data is: ");
	for(i=0; i<16; i++){
		printf("%02x", input[i]);
	}
	printf("\n");
}

void print2 (uchar* input){
	int i;
	printf("the printed data is: ");
	for(i=0; i<176; i++){
		printf("%02x", input[i]);
	}
	printf("\n");
}

void MixColumns(unsigned char* state) {

	uchar temp[16];
	int i;
	for(i = 0; i < 16; i++){
		temp[i] = state[i];
	}

		state[0]  = gf_mul[temp[0*4]][0] ^ gf_mul[temp[0*4+1]][1] ^ temp[0*4+2] ^ temp[0*4+3];
		state[1]  = temp[0*4] ^ gf_mul[temp[0*4+1]][0] ^ gf_mul[temp[0*4+2]][1] ^ temp[0*4+3];
		state[2]  = temp[0*4] ^ temp[0*4+1] ^ gf_mul[temp[0*4+2]][0] ^ gf_mul[temp[0*4+3]][1];
		state[3]  = gf_mul[temp[0*4]][1] ^ temp[0*4+1] ^ temp[0*4+2] ^ gf_mul[temp[0*4+3]][0];

		state[4]  = gf_mul[temp[1*4]][0] ^ gf_mul[temp[1*4+1]][1] ^ temp[1*4+2] ^ temp[1*4+3];
		state[5]  = temp[1*4] ^ gf_mul[temp[1*4+1]][0] ^ gf_mul[temp[1*4+2]][1] ^ temp[1*4+3];
		state[6]  = temp[1*4] ^ temp[1*4+1] ^ gf_mul[temp[1*4+2]][0] ^ gf_mul[temp[1*4+3]][1];
		state[7]  = gf_mul[temp[1*4]][1] ^ temp[1*4+1] ^ temp[1*4+2] ^ gf_mul[temp[1*4+3]][0];

		state[8]  = gf_mul[temp[2*4]][0] ^ gf_mul[temp[2*4+1]][1] ^ temp[2*4+2] ^ temp[2*4+3];
		state[9]  = temp[2*4] ^ gf_mul[temp[2*4+1]][0] ^ gf_mul[temp[2*4+2]][1] ^ temp[2*4+3];
		state[10] = temp[2*4] ^ temp[2*4+1] ^ gf_mul[temp[2*4+2]][0] ^ gf_mul[temp[2*4+3]][1];
		state[11] = gf_mul[temp[2*4]][1] ^ temp[2*4+1] ^ temp[2*4+2] ^ gf_mul[temp[2*4+3]][0];


		state[12] = gf_mul[temp[3*4]][0] ^ gf_mul[temp[3*4+1]][1] ^ temp[3*4+2] ^ temp[3*4+3];
		state[13] = temp[3*4] ^ gf_mul[temp[3*4+1]][0] ^ gf_mul[temp[3*4+2]][1] ^ temp[3*4+3];
		state[14] = temp[3*4] ^ temp[3*4+1] ^ gf_mul[temp[3*4+2]][0] ^ gf_mul[temp[3*4+3]][1];
		state[15] = gf_mul[temp[3*4]][1] ^ temp[3*4+1] ^ temp[3*4+2] ^ gf_mul[temp[3*4+3]][0];
}

void AddRoundKey(unsigned char* state, unsigned char* roundkey){
	int i;
	for(i = 0; i < 16; i++){
		state[i] ^= roundkey[i];
	}
}





/** encrypt
 *  Perform AES encryption in software.
 *
 *  Input: msg_ascii - Pointer to 32x 8-bit char array that contains the input message in ASCII format
 *         key_ascii - Pointer to 32x 8-bit char array that contains the input key in ASCII format
 *  Output:  msg_enc - Pointer to 4x 32-bit int array that contains the encrypted message
 *               key - Pointer to 4x 32-bit int array that contains the input key
 */
void encrypt(unsigned char * msg_ascii, unsigned char * key_ascii, unsigned int * msg_enc, unsigned int * key)
{
	// Implement this function
	// Implement this function
		int i;

		unsigned char word [176];
		unsigned char msg_conv[16];
		unsigned char key_conv[16];

		for(i=0; i < 16; i++){
			int j = 2*i;
			msg_conv[i] = charsToHex(msg_ascii[j], msg_ascii[j+1]);
			key_conv[i] = (unsigned char)charsToHex((char)key_ascii[j], (char)key_ascii[j+1]);
		}
		print(key_conv);


		//Initial Key Expansion WoaW
		KeyExpansion(key_conv,word);
//		print2(word);

		//IntialRound - Round 1 FIGHT!!
		AddRoundKey(msg_conv,word);
		//print(msg_conv);
		//ROUNDS 1 - 10
		for(i = 1; i < 10; i++){
			SubBytes(msg_conv);
//			printf("subByte :\n");
//			print(msg_conv);
			ShiftRows(msg_conv);
//			printf("ShiftRows :\n");
//			print(msg_conv);
			MixColumns(msg_conv);
//			printf("MixColumns :\n");
//			print(msg_conv);
			AddRoundKey(msg_conv, word + 16*(i)); // start at 1 end at 9
//			printf("AddRoundKey :\n");
//			print(msg_conv);
		}

		//Final Round 11 - TING TING TING
		printf("this is the final state");
		print(msg_conv);
		SubBytes(msg_conv);
			printf("subByte :\n");
			print(msg_conv);
		ShiftRows(msg_conv);
			printf("ShiftRows :\n");
			print(msg_conv);
		AddRoundKey(msg_conv, word + 160);
			printf("Final Value: \n");
			print(msg_conv);
			print(key_conv);

		for(i = 0; i < 4; i++){
			unsigned int test1 = msg_conv[4*i] << 24;
			//printf(%x\n, test1);
			unsigned int test2 = msg_conv[4*i + 1] << 16;
			//printf(%x\n, test2);
			unsigned int test3 = msg_conv[4*i + 2] << 8;
			//printf(%x\n, test3);
			unsigned int test4 = msg_conv[4*i +3];
			//printf(%x\n, test4);
			msg_enc[i] = test1 + test2 + test3 + test4;
			}

//		for(i = 0; i < 4; i++){
//			unsigned int test1 = key_conv[4*i] << 24;
//			printf(" test1 : %x\n", test1);
//			unsigned int test2 = key_conv[4*i + 1] << 16;
//			printf(" test2 : %x\n", test2);
//			unsigned int test3 = key_conv[4*i + 2] << 8;
//			printf(" test3 : %x\n", test3);
//			unsigned int test4 = key_conv[4*i +3];
//			printf(" test4 : %x\n", test4);
//			key[i] = test1 + test2 + test3 + test4;
//			}
		key[0] = ((unsigned int)key_conv[0]<<24) | ((unsigned int)key_conv[0]<<16)| ((unsigned int)key_conv[0]<<8) | ((unsigned int)key_conv[0]<<0);
//				| ((unsigned int)key_conv[1]<<16)|((unsigned int)key_conv[2]<<8) | ((unsigned int)key_conv[3]));
//		key[1] = ((unsigned int)key_conv[0]<<24) | ((unsigned int)key_conv[1]<<16)|((unsigned int)key_conv[2]<<8) | ((unsigned int)key_conv[3]));
//		key[2] = ((unsigned int)key_conv[0]<<24) | ((unsigned int)key_conv[1]<<16)|((unsigned int)key_conv[2]<<8) | ((unsigned int)key_conv[3]));
//		key[3] = ((unsigned int)key_conv[0]<<24) | ((unsigned int)key_conv[1]<<16)|((unsigned int)key_conv[2]<<8) | ((unsigned int)key_conv[3]));

		printf("testing key : \n");
		printf("%x", key[0]);
		printf("testing key1 : \n");
		printf("%x", key[1]);
		printf("testing key2 : \n");
		printf("%x", key[2]);
		printf("testing key3 : \n");
		printf("%x", key[3]);

}

/** decrypt
 *  Perform AES decryption in hardware.
 *
 *  Input:  msg_enc - Pointer to 4x 32-bit int array that contains the encrypted message
 *              key - Pointer to 4x 32-bit int array that contains the input key
 *  Output: msg_dec - Pointer to 4x 32-bit int array that contains the decrypted message
 */
void decrypt(unsigned int * msg_enc, unsigned int * msg_dec, unsigned int * key)
{
	// Implement this function
}

/** main
 *  Allows the user to enter the message, key, and select execution mode
 *
 */
int main()
{

	// Input Message and Key as 32x 8-bit ASCII Characters ([33] is for NULL terminator)
	unsigned char msg_ascii[33];
	unsigned char msg_ascii_week1[33];
	unsigned char key_ascii[33];
	// Key, Encrypted Message, and Decrypted Message in 4x 32-bit Format to facilitate Read/Write to Hardware
	unsigned int key[4];
	unsigned int msg_enc[4];
	unsigned int msg_dec[4];

	printf("Select execution mode: 0 for testing, 1 for benchmarking: ");
	scanf("%d", &run_mode);


	if (run_mode == 0) {
		// Continuously Perform Encryption and Decryption
		while (1) {
			int i = 0;
			printf("\nEnter Message:\n");
			scanf("%s", msg_ascii);
			printf("\n");
			printf("\nEnter Key:\n");
			scanf("%s", key_ascii);
			printf("\n");
			encrypt(msg_ascii, key_ascii, msg_enc, key);
			printf("\nEncrpted message is: \n");
			for(i = 0; i < 4; i++){
				printf("%08x", msg_enc[i]);
			}

			printf("\n");
			decrypt(msg_enc, msg_dec, key);
			printf("\nDecrypted message is: \n");
			for(i = 0; i < 4; i++){
				//printf("%08x", msg_dec[i]);
				AES_PTR[0] = key[0];
				AES_PTR[1] = key[1];
				AES_PTR[2] = key[2];
				AES_PTR[3] = key[3];
				AES_PTR[4] = msg_enc[0];
				AES_PTR[5] = msg_enc[1];
				AES_PTR[6] = msg_enc[2];
				AES_PTR[7] = msg_enc[3];
//				printf("ahhhhh");
				AES_PTR[10] = 0xDEADBEEF;
				if(AES_PTR[10] != 0xDEADBEEF){
					printf("Error !");
				}
			}
			printf("\n");
		}
	}
	else {
		// Run the Benchmark
		int i = 0;
		int size_KB = 2;
		// Choose a random Plaintext and Key
		for (i = 0; i < 32; i++) {
			msg_ascii[i] = 'ece298dcece298dcece298dcece298dc';
			key_ascii[i] = '000102030405060708090a0b0c0d0e0f';
		}
		// Run Encryption
		clock_t begin = clock();
		for (i = 0; i < size_KB * 64; i++)
			encrypt(msg_ascii, key_ascii, msg_enc, key);
		clock_t end = clock();
		double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		double speed = size_KB / time_spent;
		printf("Software Encryption Speed: %f KB/s \n", speed);
		// Run Decryption
		begin = clock();
		for (i = 0; i < size_KB * 64; i++)
			decrypt(msg_enc, msg_dec, key);
		end = clock();
		time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		speed = size_KB / time_spent;
		printf("Hardware Encryption Speed: %f KB/s \n", speed);
	}
	return 0;
}
