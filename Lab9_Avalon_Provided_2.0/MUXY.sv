//module MUX5_1(
//		input logic[127:0] TO_MUX_ADDROUNDKEY, TO_MUX_INVSUBBYTES, TO_MUX_SHIFTROWS, TO_MUX_IMC, TEMP_1, AES_MSG_ENC,
//		input logic [2:0] SELECT_REG,
//		output logic[127:0] TEMP);
//	
//	
//always_comb
//begin 
//	case(SELECT_REG)
//		3'b000:
//			TEMP = TO_MUX_ADDROUNDKEY;
//		3'b001:
//			TEMP = TO_MUX_INVSUBBYTES;
//		3'b010:
//			TEMP = TO_MUX_SHIFTROWS;
//		3'b011:
//			TEMP = TO_MUX_IMC;
//		3'b100:
//			TEMP = TEMP_1;  
//		3'b101:
//			TEMP = AES_MSG_ENC;
//		default:
//			TEMP = TEMP_1;
//	endcase
//end
//
//endmodule


