 /************************************************************************
AES Decryption Core Logic

Dong Kai Wang, Fall 2017

For use with ECE 385 Experiment 9
University of Illinois ECE Department
************************************************************************/


module AES (
	input	 logic CLK,
	input  logic RESET,
	input  logic AES_START,
	output logic AES_DONE,
	input  logic [127:0] AES_KEY,
	input  logic [127:0] AES_MSG_ENC,
	output logic [127:0] AES_MSG_DEC
);
	logic [127:0] State, invshiftrows_out, invmixcolumns_out, invsubbytes_out;
	logic[1407:0] keySchedule;
	logic [2:0] control;
	logic [4:0] round;
	logic [1:0] colnumber;
	logic [31:0] col_in, col_out;
	
	KeyExpansion keyexpansion(.clk(CLK), .Cipherkey(AES_KEY), .KeySchedule(keySchedule));
	InvShiftRows invshiftrows(.data_in(State), .data_out(invshiftrows_out));
	InvMixColumns invmixcolumns(.in(col_in), .out(col_out));
	InvSubBytes byte0(.clk(CLK), .in(State[7:0]), .out(invsubbytes_out[7:0]));
	InvSubBytes byte1(.clk(CLK), .in(State[15:8]), .out(invsubbytes_out[15:8]));
	InvSubBytes byte2(.clk(CLK), .in(State[23:16]), .out(invsubbytes_out[23:16]));
	InvSubBytes byte3(.clk(CLK), .in(State[31:24]), .out(invsubbytes_out[31:24]));
	InvSubBytes byte4(.clk(CLK), .in(State[39:32]), .out(invsubbytes_out[39:32]));
	InvSubBytes byte5(.clk(CLK), .in(State[47:40]), .out(invsubbytes_out[47:40]));
	InvSubBytes byte6(.clk(CLK), .in(State[55:48]), .out(invsubbytes_out[55:48]));
	InvSubBytes byte7(.clk(CLK), .in(State[63:56]), .out(invsubbytes_out[63:56]));
	InvSubBytes byte8(.clk(CLK), .in(State[71:64]), .out(invsubbytes_out[71:64]));
	InvSubBytes byte9(.clk(CLK), .in(State[79:72]), .out(invsubbytes_out[79:72]));
	InvSubBytes byte10(.clk(CLK), .in(State[87:80]), .out(invsubbytes_out[87:80]));
	InvSubBytes byte11(.clk(CLK), .in(State[95:88]), .out(invsubbytes_out[95:88]));
	InvSubBytes byte12(.clk(CLK), .in(State[103:96]), .out(invsubbytes_out[103:96]));
	InvSubBytes byte13(.clk(CLK), .in(State[111:104]), .out(invsubbytes_out[111:104]));
	InvSubBytes byte14(.clk(CLK), .in(State[119:112]), .out(invsubbytes_out[119:112]));
	InvSubBytes byte15(.clk(CLK), .in(State[127:120]), .out(invsubbytes_out[127:120]));
	
	
	assign AES_MSG_DEC = State;
	
	always_ff @ (posedge CLK) begin
		if(RESET)
			State <= 0;
		else if(control == 3'b100)
			State <= State;
		else if(control == 3'b111)
			State <= AES_MSG_ENC;
		else if(control == 3'b000) begin
			case(round)
				4'b0000 : 
					State <= State ^ keySchedule[127:0];
				4'b0001 :
					State <= State ^ keySchedule[255:128];
				4'b0010 :
					State <= State ^ keySchedule[383:256];
				4'b0011 :
					State <= State ^ keySchedule[511:384];
				4'b0100 :
					State <= State ^ keySchedule[639:512];
				4'b0101 :
					State <= State ^ keySchedule[767:640];
				4'b0110 :
					State <= State ^ keySchedule[895:768];
				4'b0111 :
					State <= State ^ keySchedule[1023:896];
				4'b1000 :
					State <= State ^ keySchedule[1151:1024];
				4'b1001 :
					State <= State ^ keySchedule[1279:1152];
				4'b1010 :
					State <= State ^ keySchedule[1407:1280];
			endcase
		end
		else if(control == 3'b001)
			State <= invshiftrows_out;
		else if(control == 3'b010)
		begin
			case(colnumber)
				2'b00:
					State[127:96] <= col_out;
				2'b01:
					State[95:64] <= col_out;
				2'b10:
					State[63:32] <= col_out;
				2'b11:
					State[31:0] <= col_out;
			endcase
		end	
		else if(control == 3'b011)
			State <= invsubbytes_out;

	end
	
	always_comb begin
	   col_in = 0;
		if(colnumber == 2'b00)
			col_in = State[127:96];
		if(colnumber == 2'b01)
			col_in = State[95:64];
		if(colnumber == 2'b10)
			col_in = State[63:32];
		if(colnumber == 2'b11)
			col_in = State[31:0];
	end
	
	enum logic [7:0] {WAIT,
							START,
							ADD0,
							SHIFT1,
							SUB11,
							ADD1,
							MIX11,
							MIX12,
							MIX13,
							MIX14,
							SHIFT2,
							SUB21,
							ADD2,
							MIX21,
							MIX22,
							MIX23,
							MIX24,
							SHIFT3,
							SUB31,
							ADD3,
							MIX31,
							MIX32,
							MIX33,
							MIX34,
							SHIFT4,
							SUB41,
							ADD4,
							MIX41,
							MIX42,
							MIX43,
							MIX44,
							SHIFT5,
							SUB51,
							ADD5,
							MIX51,
							MIX52,
							MIX53,
							MIX54,
							SHIFT6,
							SUB61,
							ADD6,
							MIX61,
							MIX62,
							MIX63,
							MIX64,
							SHIFT7,
							SUB71,
							ADD7,
							MIX71,
							MIX72,
							MIX73,
							MIX74,
							SHIFT8,
							SUB81,
							ADD8,
							MIX81,
							MIX82,
							MIX83,
							MIX84,
							SHIFT9,
							SUB91,
							ADD9,
							MIX91,
							MIX92,
							MIX93,
							MIX94,
							SHIFT10,
							SUB101,
							ADD10,
							DONE} Cur_State, Next_state; //State machien states 
	
	always_ff @ (posedge CLK) begin
		if(RESET)
			Cur_State <= WAIT;
		else
			Cur_State <= Next_state;
	end
	
	always_comb
	begin
		AES_DONE = 0;
		control = 3'b100;
		round = 0;
		colnumber = 0;
	
		unique case (Cur_State)
			WAIT: 
				if(AES_START)
					Next_state = START;
				else
					Next_state = WAIT;
			START:
				Next_state = ADD0;
			ADD0: 
				Next_state = SHIFT1;
			SHIFT1:
				Next_state = SUB11;
			SUB11:
				Next_state = ADD1;
			ADD1:
				Next_state = MIX11;
			MIX11:
				Next_state = MIX12;
			MIX12:
				Next_state = MIX13;
			MIX13:
				Next_state = MIX14;
			MIX14: 
				Next_state = SHIFT2;
			SHIFT2:
				Next_state = SUB21;
			SUB21:
				Next_state = ADD2;
			ADD2:
				Next_state = MIX21;
			MIX21:
				Next_state = MIX22;
			MIX22:
				Next_state = MIX23;
			MIX23:
				Next_state = MIX24;
			MIX24: 
				Next_state = SHIFT3;
			SHIFT3:
				Next_state = SUB31;
			SUB31:
				Next_state = ADD3;
			ADD3:
				Next_state = MIX31;
			MIX31:
				Next_state = MIX32;
			MIX32:
				Next_state = MIX33;
			MIX33:
				Next_state = MIX34;
			MIX34: 
				Next_state = SHIFT4;
			SHIFT4:
				Next_state = SUB41;
			SUB41:
				Next_state = ADD4;
			ADD4:
				Next_state = MIX41;
			MIX41:
				Next_state = MIX42;
			MIX42:
				Next_state = MIX43;
			MIX43:
				Next_state = MIX44;
			MIX44: 
				Next_state = SHIFT5;
			SHIFT5:
				Next_state = SUB51;
			SUB51:
				Next_state = ADD5;
			ADD5:
				Next_state = MIX51;
			MIX51:
				Next_state = MIX52;
			MIX52:
				Next_state = MIX53;
			MIX53:
				Next_state = MIX54;
			MIX54: 
				Next_state = SHIFT6;
			SHIFT6:
				Next_state = SUB61;
			SUB61:
				Next_state = ADD6;
			ADD6:
				Next_state = MIX61;
			MIX61:
				Next_state = MIX62;
			MIX62:
				Next_state = MIX63;
			MIX63:
				Next_state = MIX64;
			MIX64: 
				Next_state = SHIFT7;
			SHIFT7:
				Next_state = SUB71;
			SUB71:
				Next_state = ADD7;
			ADD7:
				Next_state = MIX71;
			MIX71:
				Next_state = MIX72;
			MIX72:
				Next_state = MIX73;
			MIX73:
				Next_state = MIX74;
			MIX74: 
				Next_state = SHIFT8;
			SHIFT8:
				Next_state = SUB81;
			SUB81:
				Next_state = ADD8;
			ADD8:
				Next_state = MIX81;
			MIX81:
				Next_state = MIX82;
			MIX82:
				Next_state = MIX83;
			MIX83:
				Next_state = MIX84;
			MIX84: 
				Next_state = SHIFT9;
			SHIFT9:
				Next_state = SUB91;
			SUB91:
				Next_state = ADD9;
			ADD9:
				Next_state = MIX91;
			MIX91:
				Next_state = MIX92;
			MIX92:
				Next_state = MIX93;
			MIX93:
				Next_state = MIX94;
			MIX94: 
				Next_state = SHIFT10;
			SHIFT10:
				Next_state = SUB101;
			SUB101:
				Next_state = ADD10;
			ADD10:
				Next_state = DONE;
			DONE:
				if(AES_START == 0)
					Next_state = WAIT;
				else
					Next_state = DONE;
		endcase
		
		case (Cur_State)
			WAIT: ;
			START: 
				control = 3'b111;
			ADD0:
				control = 3'b000;
			SHIFT1:
				control = 3'b001;
			SUB11:
				control = 3'b011;
			ADD1:
				begin
					control = 3'b000;
					round = 4'b0001;
				end
			MIX11:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX12:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX13:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX14:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end
			SHIFT2:
				control = 3'b001;
			SUB21:
				control = 3'b011;
			ADD2:
				begin
					control = 3'b000;
					round = 4'b0010;
				end
			MIX21:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX22:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX23:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX24:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end
			SHIFT3:
				control = 3'b001;
			SUB31:
				control = 3'b011;
			ADD3:
				begin
					control = 3'b000;
					round = 4'b0011;
				end
			MIX31:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX32:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX33:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX34:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end
			SHIFT4:
				control = 3'b001;
			SUB41:
				control = 3'b011;
			ADD4:
				begin
					control = 3'b000;
					round = 4'b0100;
				end
			MIX41:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX42:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX43:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX44:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end
			SHIFT5:
				control = 3'b001;
			SUB51:
				control = 3'b011;
			ADD5:
				begin
					control = 3'b000;
					round = 4'b0101;
				end
			MIX51:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX52:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX53:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX54:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end	
			SHIFT6:
				control = 3'b001;
			SUB61:
				control = 3'b011;
			ADD6:
				begin
					control = 3'b000;
					round = 4'b0110;
				end
			MIX61:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX62:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX63:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX64:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end
			SHIFT7:
				control = 3'b001;
			SUB71:
				control = 3'b011;
			ADD7:
				begin
					control = 3'b000;
					round = 4'b0111;
				end
			MIX71:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX72:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX73:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX74:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end
			SHIFT8:
				control = 3'b001;
			SUB81:
				control = 3'b011;
			ADD8:
				begin
					control = 3'b000;
					round = 4'b1000;
				end
			MIX81:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX82:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX83:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX84:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end
			SHIFT9:
				control = 3'b001;
			SUB91:
				control = 3'b011;
			ADD9:
				begin
					control = 3'b000;
					round = 4'b1001;
				end
			MIX91:
				begin
					control = 3'b010;
					colnumber = 2'b00;
				end
			MIX92:
				begin
					control = 3'b010;
					colnumber = 2'b01;
				end
			MIX93:
				begin
					control = 3'b010;
					colnumber = 2'b10;
				end
			MIX94:
				begin
					control = 3'b010;
					colnumber = 2'b11;
				end
			SHIFT10:
				control = 3'b001;
			SUB101:
				control = 3'b011;
			ADD10:
				begin
					control = 3'b000;
					round = 4'b1010;
				end
			DONE:
				begin
					AES_DONE = 1;
				end

		endcase
	end

endmodule
